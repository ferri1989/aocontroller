﻿using AOController.Data;
using AOController.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOController.Services
{
    public class ImportLogServices : IImportLogService
    {

        private DbContextOptions<GameDbContext> _dbContextOptions;
        public ImportLogServices(DbContextOptions<GameDbContext> dbContextOptions)
        {
            _dbContextOptions = dbContextOptions;
        }
        public async Task<bool> RegisterTransactionLog(ImportLogModel importLogModel)
        {
            using (var db = new GameDbContext(_dbContextOptions))
            {
                var exist = db.ImportLogModels.FirstOrDefault(x => x.Date == importLogModel.Date && x.Item == importLogModel.Item && x.Player == importLogModel.Player);
                if (exist == null)
                    db.ImportLogModels.Add(importLogModel);
                await db.SaveChangesAsync();
                return true;
            }
        }

        public Task UpdateUser(ImportLogModel user)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ImportLogModel>> All()
        {
            using (var db = new GameDbContext(_dbContextOptions))
            {
                return Task.FromResult<IEnumerable<ImportLogModel>> (db.ImportLogModels.OrderByDescending(o => o.Date).ToList());
            }
        }

        public Task<IEnumerable<ImportLogModel>> AllBetweenDates(DateTime startDate, DateTime endDate)
        {
            using (var db = new GameDbContext(_dbContextOptions))
            {
                return Task.FromResult<IEnumerable<ImportLogModel>>(db.ImportLogModels.Where(ilm => ilm.Date>= startDate && ilm.Date <= endDate).OrderByDescending(o => o.Date).ToList());
            }
        }

    }
}
