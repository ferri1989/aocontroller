﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOController.Services
{
    public interface IDonationsCheckService
    {
        Task<bool> AddDonation(Models.DonationsCheckModel donation);

        Task<IEnumerable<Models.DonationsCheckModel>> All();
    }
}
