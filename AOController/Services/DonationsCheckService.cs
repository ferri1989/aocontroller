﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOController.Data;
using AOController.Models;
using Microsoft.EntityFrameworkCore;

namespace AOController.Services
{
    public class DonationsCheckService : IDonationsCheckService
    {
        private DbContextOptions<GameDbContext> _dbContextOptions;
        public DonationsCheckService(DbContextOptions<GameDbContext> dbContextOptions)
        {
            _dbContextOptions = dbContextOptions;
        }
        public async Task<bool> AddDonation(DonationsCheckModel donation)
        {
            using (var db = new GameDbContext(_dbContextOptions))
            {
                var exist = db.DonationCheckModel.FirstOrDefault(x => x.Player == donation.Player && x.WeekOfYear == donation.WeekOfYear && x.Year == donation.Year);
                if (exist == null)
                    db.DonationCheckModel.Add(donation);
                await db.SaveChangesAsync();
                return true;
            }
        }

        public Task<IEnumerable<DonationsCheckModel>> All()
        {
            using (var db = new GameDbContext(_dbContextOptions))
            {
                return Task.FromResult<IEnumerable<DonationsCheckModel>>(db.DonationCheckModel.ToList());
            }
        }
    }
}
