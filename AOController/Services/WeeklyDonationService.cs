﻿using AOController.Data;
using AOController.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOController.Services
{
    public class WeeklyDonationService : IWeeklyDonationService
    {
        private DbContextOptions<GameDbContext> _dbContextOptions;
        public WeeklyDonationService(DbContextOptions<GameDbContext> dbContextOptions)
        {
            _dbContextOptions = dbContextOptions;
        }

        public Task<IEnumerable<WeeklyDonationItemModel>> All()
        {
            using (var db = new GameDbContext(_dbContextOptions))
            {
                return Task.FromResult<IEnumerable<WeeklyDonationItemModel>>(db.WeeklyDonationItemModel.ToList());
            }
        }

        //public Task<IEnumerable<object>> AllWithDescriptions()
        //{
        //    using (var db = new GameDbContext(_dbContextOptions))
        //    {
        //        return Task.FromResult<IEnumerable<object>>(db.WeeklyDonationItemModel.ToList().Select(x => new { ItemCode = x.WeeklyItemId, ItemDescription = x.WeeklyItem.Description }));
        //    }
        //}

        public Task<IEnumerable<object>> AllWithDescriptions()
        {
            using (var db = new GameDbContext(_dbContextOptions))
            {
                object resultWithDescription;
                var result = db.WeeklyDonationItemModel.ToList();
                foreach (var r in result)
                {
                    r.WeeklyItem = db.ItemsModels.FirstOrDefault(x => x.Id == r.WeeklyItemId);
                }
                return Task.FromResult<IEnumerable<object>>(result);
            }

        }
    }
}
