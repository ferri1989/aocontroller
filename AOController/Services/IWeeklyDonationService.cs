﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOController.Models;

namespace AOController.Services
{
    public interface IWeeklyDonationService 
    {
        Task<IEnumerable<WeeklyDonationItemModel>> All();

        Task<IEnumerable<object>> AllWithDescriptions();
    }
}
