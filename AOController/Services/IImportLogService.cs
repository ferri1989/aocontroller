﻿using AOController.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AOController.Services
{
    public interface IImportLogService
    {

        Task<bool> RegisterTransactionLog(ImportLogModel userModel);

        Task UpdateUser(ImportLogModel user);

        Task<IEnumerable<ImportLogModel>> All();
        Task<IEnumerable<ImportLogModel>> AllBetweenDates(DateTime startDate, DateTime endDate);


    }
}
