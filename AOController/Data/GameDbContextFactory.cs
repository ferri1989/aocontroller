﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace AOController.Data
{
    public class GameDbContextFactory :  IDesignTimeDbContextFactory<GameDbContext>
    {
        public GameDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder =
             new DbContextOptionsBuilder<GameDbContext>();
            //optionsBuilder.UseSqlServer(@"Server= (localdb)\MSSQLLocalDB;Database=AOController; Trusted_Connection=True;MultipleActiveResultSets=true");
            optionsBuilder.UseSqlServer(@"Server=35.198.147.41;Database=AOController;MultipleActiveResultSets=true;User Id=SA;Password=x10nr2fL2");
            return new GameDbContext(optionsBuilder.Options);
        }
    }
}
