﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AOController.Models;
using AOController.Extensions;

namespace AOController.Data
{
    public class GameDbContext : DbContext
    {
        public DbSet<ImportLogModel> ImportLogModels { get; set; }

        public DbSet<ItemsModel> ItemsModels { get; set; }

        public DbSet<WeeklyDonationItemModel> WeeklyDonationItemModel { get; set; }

        public DbSet<PlayerModel> PlayerModel { get; set; }
        public DbSet<DonationsCheckModel> DonationCheckModel { get; set; }
        public GameDbContext(DbContextOptions<GameDbContext> dbContextOptions) : base (dbContextOptions)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.RemovePluralizingTableNameConvention();
            //modelBuilder.Entity(typeof(GameSessionModel))
            //.HasOne(typeof(UserModel), "User2")
            //.WithMany()
            //.HasForeignKey("User2Id").OnDelete(DeleteBehavior.Restrict);
        }
    }
}
