﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AOController.Migrations
{
    public partial class check_donation_model : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ItemId",
                table: "DonationsCheckModel",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_DonationsCheckModel_ItemId",
                table: "DonationsCheckModel",
                column: "ItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_DonationsCheckModel_ItemsModel_ItemId",
                table: "DonationsCheckModel",
                column: "ItemId",
                principalTable: "ItemsModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DonationsCheckModel_ItemsModel_ItemId",
                table: "DonationsCheckModel");

            migrationBuilder.DropIndex(
                name: "IX_DonationsCheckModel_ItemId",
                table: "DonationsCheckModel");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "DonationsCheckModel");
        }
    }
}
