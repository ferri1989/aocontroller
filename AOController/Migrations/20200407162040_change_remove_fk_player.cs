﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AOController.Migrations
{
    public partial class change_remove_fk_player : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DonationsCheckModel_PlayerModel_PlayerId",
                table: "DonationsCheckModel");

            migrationBuilder.DropIndex(
                name: "IX_DonationsCheckModel_PlayerId",
                table: "DonationsCheckModel");

            migrationBuilder.DropColumn(
                name: "PlayerId",
                table: "DonationsCheckModel");

            migrationBuilder.AddColumn<string>(
                name: "Player",
                table: "DonationsCheckModel",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Player",
                table: "DonationsCheckModel");

            migrationBuilder.AddColumn<long>(
                name: "PlayerId",
                table: "DonationsCheckModel",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_DonationsCheckModel_PlayerId",
                table: "DonationsCheckModel",
                column: "PlayerId");

            migrationBuilder.AddForeignKey(
                name: "FK_DonationsCheckModel_PlayerModel_PlayerId",
                table: "DonationsCheckModel",
                column: "PlayerId",
                principalTable: "PlayerModel",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
