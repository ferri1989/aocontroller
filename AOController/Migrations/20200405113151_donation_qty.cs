﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AOController.Migrations
{
    public partial class donation_qty : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Qty",
                table: "WeeklyDonationItemModel",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Qty",
                table: "WeeklyDonationItemModel");
        }
    }
}
