﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AOController.Migrations
{
    public partial class fk_itemswee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "WeeklyDonationItemModel",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WeeklyItemId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WeeklyDonationItemModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_WeeklyDonationItemModel_ItemsModel_WeeklyItemId",
                        column: x => x.WeeklyItemId,
                        principalTable: "ItemsModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WeeklyDonationItemModel_WeeklyItemId",
                table: "WeeklyDonationItemModel",
                column: "WeeklyItemId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WeeklyDonationItemModel");
        }
    }
}
