﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace AOController.Migrations
{
    public partial class donation_check_weekly : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DonationsCheckModel",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Year = table.Column<int>(nullable: false),
                    WeekOfYear = table.Column<int>(nullable: false),
                    PlayerId = table.Column<long>(nullable: false),
                    DonationCheck = table.Column<float>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DonationsCheckModel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DonationsCheckModel_PlayerModel_PlayerId",
                        column: x => x.PlayerId,
                        principalTable: "PlayerModel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DonationsCheckModel_PlayerId",
                table: "DonationsCheckModel",
                column: "PlayerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DonationsCheckModel");
        }
    }
}
