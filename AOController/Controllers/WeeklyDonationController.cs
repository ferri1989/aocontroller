﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using AOController.Models;
using AOController.Services;
using Microsoft.AspNetCore.Mvc;

namespace AOController.Controllers
{
    public class WeeklyDonationController : Controller
    {

        private IImportLogService _impoterService;
        private IWeeklyDonationService _weeklyDonationService;
        private IDonationsCheckService _donationCheckService;

        public WeeklyDonationController(IImportLogService impoterService, IWeeklyDonationService weeklyDonationService, IDonationsCheckService donationCheckService)
        {
            _impoterService = impoterService;
            _weeklyDonationService = weeklyDonationService;
            _donationCheckService = donationCheckService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> List()
        {
            var rows = await _donationCheckService.All();
            return View(rows);
        }
        public async Task<IActionResult> CheckWeek(int week)
        {
            if (await calculateDonationByWeek(week))
            {
                return Content(string.Format("Semana {0} recalculada", week));
            }
            else
            {
                return Content(string.Format("Error calculado donaciones de la semana {0}", week));
            }
            
        }

        private async Task<bool> calculateDonationByWeek(int week)
        {
            DateTime startDay;
            DateTime endDay;
            startDay = FirstDateOfWeekISO8601(DateTime.Now.Year, week);
            endDay = startDay.AddDays(7);
            endDay = endDay.AddHours(23);
            endDay = endDay.AddMinutes(59);
            endDay = endDay.AddSeconds(59);
            var lstTransLogs = await _impoterService.AllBetweenDates(startDay, endDay);
            //Obtenemos el sumatorio por item y jugador de movimientos.
            var donationsPlayerItem = lstTransLogs.GroupBy(x => new { x.Player, x.Item })
                   .Select(g => new { g.Key.Player, g.Key.Item, MyCount = g.Sum(l => l.Amount) });
            //Obtenemos los items que cuentan para la donación.
            var donationListItems = _weeklyDonationService.AllWithDescriptions();
            List<Models.ImportLogModel> lsTransWithDonations;
            //Recorremos cada jugador que ha realizado donaciones
            foreach(var player in donationsPlayerItem.GroupBy(x => x.Player))
            {
                //Nueva lista con los que cuentan para la donacion
                foreach (WeeklyDonationItemModel item in donationListItems.Result)
                {
                    //ImportLogModel donation = donationsPlayerItem.FirstOrDefault(x => x.Item == item.WeeklyItem.Description);
                    var donation = donationsPlayerItem.FirstOrDefault(x => x.Item == item.WeeklyItem.Description);
                    if (donation != null )
                    {
                        //Guardamos la donación
                        DonationsCheckModel wdim = new DonationsCheckModel();
                        wdim.DonationCheck = donation.MyCount / item.Qty;
                        wdim.ItemId = item.WeeklyItemId;
                        wdim.Player = player.Key.ToString();
                        wdim.WeekOfYear = week;
                        wdim.Year = DateTime.Now.Year;
                        await _donationCheckService.AddDonation(wdim);
                    }
                }
            }
            return true;
        }

        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            // Use first Thursday in January to get first week of the year as
            // it will never be in Week 52/53
            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            // As we're adding days to a date in Week 1,
            // we need to subtract 1 in order to get the right date for week #1
            if (firstWeek == 1)
            {
                weekNum -= 1;
            }

            // Using the first Thursday as starting week ensures that we are starting in the right year
            // then we add number of weeks multiplied with days
            var result = firstThursday.AddDays(weekNum * 7);

            // Subtract 3 days from Thursday to get Monday, which is the first weekday in ISO8601
            return result.AddDays(-3);
        }
    }
}