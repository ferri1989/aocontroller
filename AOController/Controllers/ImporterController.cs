﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AOController.Models;
using AOController.Services;

namespace AOController.Controllers
{
    public class ImporterController : Controller
    {
        private IImportLogService _impoterService;
        public ImporterController(IImportLogService impoterService)
        {
            _impoterService = impoterService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(TransactionModel model)
        {
            if (processLog(model.TransactionLog))
            {
                return RedirectToAction("List");
            }
            return View();
        }

        private bool processLog(string text)
        {

            var lineArray = text.Split(Environment.NewLine,StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in lineArray)
            {
                ImportLogModel ilm = new ImportLogModel();
                var columArray = line.Split(@"	");
                if (columArray.Length == 6 && columArray[0] != "\"Date\"" &&  columArray[0] != "\"Fecha\"")
                {
                    ilm.Date = DateTime.ParseExact(columArray[0].Replace("\"", ""), "MM/dd/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    ilm.Player = columArray[1].Replace("\"", "");
                    ilm.Item = columArray[2].Replace("\"", "");
                    ilm.Enchantment = int.Parse(columArray[3].Replace("\"", ""));
                    ilm.Quality = int.Parse(columArray[4].Replace("\"", ""));
                    ilm.Amount = int.Parse(columArray[5].Replace("\"", ""));
                    _impoterService.RegisterTransactionLog(ilm);
                }
            }
            return true;
        }

        public async Task<IActionResult> List()
        {
            var rows = await _impoterService.All();
            return View(rows);
        }
    }
}