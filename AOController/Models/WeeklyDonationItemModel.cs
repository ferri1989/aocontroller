﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AOController.Models
{
    public class WeeklyDonationItemModel
    {
        [Key]
        public long Id { get; set; }
        [Required]
        [ForeignKey(nameof(WeeklyItemId))]
        public virtual ItemsModel WeeklyItem { get; set; }
        public long WeeklyItemId { get; set; }

        public int Qty { get; set; }
    }
}
