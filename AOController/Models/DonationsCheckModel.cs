﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AOController.Models
{
    public class DonationsCheckModel
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public int Year { get; set; }
        [Required]
        public int WeekOfYear { get; set; }
        [Required]
        public string Player { get; set; }
        public float DonationCheck { get; set; }

        public ItemsModel Item { get; set; }
        [ForeignKey(nameof(ItemId))]
        public long ItemId { get; set; }
    }
}
