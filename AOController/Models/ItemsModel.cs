﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AOController.Models
{
    public class ItemsModel
    {
        [Key]
        public long Id { get; set; }
        [Required]
        public string Code { get; set; }

        public string Description { get; set; }

        public string IconRute { get; set; }

        public ICollection<WeeklyDonationItemModel> WeeklyDonationItems { get; set; }
    }
}
