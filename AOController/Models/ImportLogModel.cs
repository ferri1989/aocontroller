﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AOController.Models
{
    public class ImportLogModel
    {
        [Key]
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string Player { get; set; }
        public string Item { get; set; }
        public int Enchantment { get; set; }
        public int Quality { get; set; }
        public int Amount { get; set; }
    }
}
