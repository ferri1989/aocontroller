﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AOController.Models
{
    public class PlayerModel
    {
        [Key]
        public long Id { get; set; }
        public string NickName { get; set; }
    }
}
